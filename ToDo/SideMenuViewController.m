//
//  SideMenuViewController.m
//  ToDo
//
//  Created by Kurs on 3/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "AppDelegate.h"

@interface SideMenuViewController ()  <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *itemsArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewLeadingConstraint;
@end

@implementation SideMenuViewController
#pragma mark - Actions
- (IBAction)closeButtonTapped:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:CLOSE_SIDE_MENU_NOTIFICATION object:nil];
}
#pragma mark - View lifecycle
// Remove entry from user defaults
- (void)logOut {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_UD];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Show Login
    UINavigationController *navigationController = (UINavigationController *)[Helpers initWithControllerFrom:@"LoginNavigationController"];
    
    // Postavljamo navigation na root. ovo je nesto slicno resetovanoj aplikaciji
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = navigationController;
}
#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.itemsArray = @[@"Home", @"Walkthrough", @"Statistics",@"Add Task", @"Log out"];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.tableViewLeadingConstraint.constant = kMenuOffset;
    [self.view layoutIfNeeded];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArray.count;
}

- (SideMenuTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.subtitleLabel.hidden = (indexPath.row == 0) ? NO : YES;
    cell.subtitleLabel.text = [NSString stringWithFormat:@"%ld", [DATA_MANAGER numberOfTasksForToday]];
    cell.titleLabel.text = self.itemsArray[indexPath.row];
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self closeButtonTapped:nil];
    
    NSString *item = self.itemsArray[indexPath.row];
    if ([item isEqualToString:@"Walkthrough"]) {
        UIViewController *toViewController = [Helpers initWithControllerFrom:@"WalkthroughViewController"];
        [self.containerViewController openViewController:toViewController];
    }else if ([item isEqualToString:@"Statistics"]) {
        UIViewController *toViewController = [Helpers initWithControllerFrom:@"StatisticsViewController"];
        [self.containerViewController openViewController:toViewController];
    }else if ([item isEqualToString:@"Add Task"]) {
        UIViewController *toViewController = [Helpers initWithControllerFrom:@"TaskViewController"];
        
        // Illi preko reference ili preko notifikacije
        // Via reference
        [self.containerViewController openViewController:toViewController];
        
        // Via Notification
//        [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_VC_NOTIFICATION object:toViewController];
    }else if ([item isEqualToString:@"Log Out"]) {
        //Remove entry from user defaults
        [self logOut];
    }
}
@end
