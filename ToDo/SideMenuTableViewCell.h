//
//  SideMenuTableViewCell.h
//  ToDo
//
//  Created by Kurs on 3/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@end
