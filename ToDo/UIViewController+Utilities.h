//
//  UIViewController+Utilities.h
//  ToDo
//
//  Created by Kurs on 3/8/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utilities)
- (void)showAlertWithTitle: (NSString *)title andMessage:(NSString *)message;
@end
