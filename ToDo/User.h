//
//  User.h
//  ToDo
//
//  Created by Kurs on 3/1/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <Foundation/Foundation.h>

// NSCoding koristimo da bismo ovaj objekat mogli da dodamo u NSUserDefaults
//
@interface User : NSObject <NSCoding>
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *birthday;
@end
