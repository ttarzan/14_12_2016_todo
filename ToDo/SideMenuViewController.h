//
//  SideMenuViewController.h
//  ToDo
//
//  Created by Kurs on 3/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContainerViewController.h"

@interface SideMenuViewController : UIViewController
@property (weak, nonatomic) ContainerViewController *containerViewController;
@end
