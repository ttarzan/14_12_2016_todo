//
//  UIViewController+Utilities.m
//  ToDo
//
//  Created by Kurs on 3/8/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "UIViewController+Utilities.h"

@implementation UIViewController (Utilities)
- (void)showAlertWithTitle: (NSString *)title andMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
