//
//  TaskTableViewCell.h
//  ToDo
//
//  Created by Kurs on 3/3/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *groupView;
// ovde ide i slika, ali nismo je vezali

// ovo je klasa koju sam napravio CORE DATA
@property (strong, nonatomic) DBTask *task;
@end
