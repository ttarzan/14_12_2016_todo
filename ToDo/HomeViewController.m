//
//  HomeViewController.m
//  ToDo
//
//  Created by Kurs on 2/11/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "HomeViewController.h"
#import "DateCollectionViewCell.h"
#import "TaskTableViewCell.h"
#import "TaskViewController.h"

@interface HomeViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *noTaskLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthYearLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *datesArray;
@property (strong, nonatomic) NSMutableArray *tasksArray;
@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) DBTask *selectedTask;
@end

@implementation HomeViewController

#pragma mark - Properties
// lazy loading
- (NSMutableArray *)datesArray {
    if (!_datesArray) {
        _datesArray = [[NSMutableArray alloc] init];
    }
    return _datesArray;
}

- (void)setSelectedDate:(NSDate *)selectedDate {
    _selectedDate = selectedDate;
    
    // kada neko setuje selected date onda se pozivaju ove metode
    [self configureCalendar];
    [self configureTasks];
}
#pragma view - Actions
- (IBAction) menuButtonTapped {
    [[NSNotificationCenter defaultCenter] postNotificationName:OPEN_SIDE_MENU_NOTIFICATION object:nil];
}

- (IBAction)searchButtonTapped {
    
}

- (IBAction)previousButtonTapped {
    [self updateMonth:-1];
}

- (IBAction)nextButtonTapped {
    [self updateMonth:1];
}

- (IBAction)userImageViewTapped:(UITapGestureRecognizer *)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose source: " message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Photo library sta se desava kada korisnik klikne na photo library akciju
    UIAlertAction *photoLibraryAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        // da li se picker alocirao
        if(picker) {
            // dozvoljavamo editovanje slike
            picker.allowsEditing = YES;
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            // completion je nesto sto zelim da prikazem na kraju operacije.
            // tj. sta zelim da se uradi kada se picker pojavi
            [self presentViewController:picker animated:YES
                             completion:nil];
        }
    }];
    [alertController addAction:photoLibraryAction];
    
    // Camera
    // prvo pitamo da li kamera postoji
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertAction *camerAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            if(picker) {
                picker.allowsEditing = YES;
                picker.delegate = self;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:nil];
            }
        }];
        [alertController addAction:camerAction];
    }
    
    
    // Cancel
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma view - Private API
- (void)configureWelcomeLabel {
    if([Helpers isMorning]) {
        self.welcomeLabel.text = @"Good Morning";
    }else {
        self.welcomeLabel.text = @"Good Afternoon";
    }
}

- (void)configureTasks {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMAT;
    
    NSString *filter = [NSString stringWithFormat:@"date LIKE '%@'", [dateFormatter stringFromDate:self.selectedDate]];
    
    self.tasksArray = [DATA_MANAGER fetchEntity:NSStringFromClass(DBTask.class) withFilter:filter withSortAsc:YES forKey:@"date"];
    
    for(DBTask *task in self.tasksArray) {
        [DATA_MANAGER logObject:task];
    }
    [self.tableView reloadData];
    
    [self configureTasksLabel];
}

- (void)configureTasksLabel {
    // ispisujem broj taskova koje imam
    self.taskLabel.text = [NSString stringWithFormat:@"%ld", self.tasksArray.count];
    
    // ako nemam taskova, sakri view
    // ovo je onaj zeleni kruzic iznad slike
    self.taskLabel.hidden = (self.tasksArray.count == 0) ? YES: NO;
}

- (void)configureCalendar {
    // MMMM daje February
    // MMM daje FEB
    [self.datesArray removeAllObjects];
    self.monthYearLabel.text = [Helpers valueFrom:self.selectedDate withFormat:@"MMMM yyyy"].uppercaseString;
    
    // Get number of days for current month
    NSInteger days = [Helpers numberOfDaysInMonthForDate:self.selectedDate];
    
    for (int i = 1; i <= days; i++) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:self.selectedDate];
        dateComponents.day = i;
        NSDate *date = [calendar dateFromComponents:dateComponents];
        [self.datesArray addObject:date];
    }
    
    [self.collectionView reloadData];
    [self scrollToCurrentDay];
    
    //NACINI KAKO MOZE SVE DA SE RESI PROBLEM ZAKASNJENJA ispisivanja u collection na trenutni datum
    // svaki od njih ide sam. ovaj prvi je najlaksi.
    // [self performSelector:@selector(scrollToCurrentDay) withObject:nil afterDelay:0.25];
    // [NSThread detachNewThreadSelector:<#(nonnull SEL)#> toTarget:<#(nonnull id)#> withObject:<#(nullable id)#>]
    // [self performSelectorInBackground:<#(nonnull SEL)#> withObject:<#(nullable id)#>]
    
}

- (void)configureUserImage {
    
}

- (void)scrollToCurrentDay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSInteger currentDay = [[NSCalendar currentCalendar] component:NSCalendarUnitDay fromDate:self.selectedDate];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentDay-1 inSection:0];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    });
}

- (void)updateMonth:(NSInteger) value {
    // currentCalendar je property. Ali je ovde kao metod zato sto se koristi kao JEBENI GET!!!! getter
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:self.selectedDate];
    // ovde mozes da uradis da kada se vrati na tekuci mesec, izabere datum koji je danasnji
    dateComponents.day = 1;
    dateComponents.month += value;
    
    self.selectedDate = [calendar dateFromComponents:dateComponents];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureUserImage];
    [self configureWelcomeLabel];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    // Load image from NSUserDefaults
    if ([[NSUserDefaults standardUserDefaults] objectForKey:USER_IMAGE]) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:USER_IMAGE];
        UIImage *image = [[UIImage alloc] initWithData:data];
        self.userImageView.image = image;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.selectedDate = [NSDate date];
    self.selectedTask = nil;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // moze i ovako
    // if([segue.identifier isEqualToString:@"TaskSegue"]) {
    
    if ([segue.destinationViewController isKindOfClass:TaskViewController.class]) {
        TaskViewController *toViewController = segue.destinationViewController;
        toViewController.task = self.selectedTask;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.datesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.date = self.datesArray[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedDate = self.datesArray[indexPath.item];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasksArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.task = self.tasksArray[indexPath.row];
//    [[DataManager sharedManager] logObject:self.tasksArray[indexPath.row]];
    
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.selectedTask = self.tasksArray[indexPath.row];
    [self performSegueWithIdentifier:@"TaskSegue" sender:nil];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // brisem task iz niza, niz koristim da mi prikaze taskove
        DBTask *task = self.tasksArray[indexPath.row];
        [self.tasksArray removeObject:task];
        // ovde ga brisem iz baze
        [DATA_MANAGER deleteObject:task];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
    [self configureTasksLabel];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
    }
    self.userImageView.image = image;
    // Store image to NSUserDefaults
    // da bih mogao da ubacim sliku u NSUserDEfaults moram prvo sliku da deserijalizujem u niz bitova
    // slika ne moze da stane kao objekat u NSUserDefaults vec mora da ide kao bit
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:USER_IMAGE];
    // moram da uradim sinhronizaciju zato sto na taj nacin on sve zaustavlja i ubacuje podatke u NSUserDefaults
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // zatvaramo picker controller
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end

