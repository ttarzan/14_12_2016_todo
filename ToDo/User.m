//
//  User.m
//  ToDo
//
//  Created by Kurs on 3/1/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "User.h"

@implementation User
#pragma mark - NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.username = [aDecoder decodeObjectForKey:@"username"];
        self.password = [aDecoder decodeObjectForKey:@"password"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.birthday = [aDecoder decodeObjectForKey:@"birthday"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.username forKey:@"username"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.password forKey:@"password"];
    [aCoder encodeObject:self.birthday forKey:@"birthday"];
}
@end
