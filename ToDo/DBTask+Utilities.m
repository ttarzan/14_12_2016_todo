 //
//  DBTask+Utilities.m
//  ToDo
//
//  Created by Kurs on 3/4/17.
//  Copyright © 2017 Kurs. All rights reserved.
//


@implementation DBTask (Utilities)

#pragma mark - Public API
- (UIColor *)groupColor {
    if (self.group == TaskGroupCompleted) {
        return kColorCompleted;
    }else if (self.group == TaskGroupNotCompleted) {
        return kColorNotCompleted;
    }else {
        return kColorInProgress;
    }
}

#pragma mark - MKAnnotation
// naslov za crvenu ciodu na mapi
- (NSString *) title {
    return self.heading;
}

// podnaslov za crvenu ciodu na mapi
- (NSString *)subtitle {
    return self.desc;
}

//gde ce da ide cioda na mapi
- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}
@end
