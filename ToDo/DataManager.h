//
//  DataManager.h
//  ToDo
//
//  Created by Kurs on 2/25/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>

// sa delegatom zelim da obavestim da mi se grad promenio
// ugovor izmedju dve strane sta koja mora da ispostuje
@protocol DataManagerDelegate <NSObject>
// ako ne stavimo optional onda ta metoda mora da se implement kada se implementiramo ovaj protocol
@optional
- (void)dataManagerDidUpdateLocality;
@end

@interface DataManager : NSObject
@property (strong, nonatomic) CLLocation *userLocation;
@property (strong, nonatomic) NSString *userLocality;
@property (weak, nonatomic) id<DataManagerDelegate> delegate;
// Singleton
// ovde cu da smestam poslednju aktivnu user lokaciju
+ (instancetype)sharedManager;


// Operacije koje cu da radim na bazi. NSManagedObject je root u CoreData
// fetchEntity - metod sa kojim uzimam podatke iz baze
// withFilter - ovo je where iz SQL-a
// withSortAsc - ASC, DESC
// forKey - WHERE <ovo je key> iz SQL-a
- (NSMutableArray *)fetchEntity:(NSString *)entityName
                     withFilter:(NSString *)filter
                    withSortAsc:(BOOL)sortAscending
                         forKey:(NSString *)sortKey;
- (void)deleteObject:(NSManagedObject *)object;
- (void)updateObject:(NSManagedObject *)object;
- (void)logObject:(NSManagedObject *)object;
// broj taskova po grupi
- (NSInteger)numberOfTasksPerTaskGroup:(TaskGroup)group;
- (void)saveTaskWithTitle:(NSString *)title
              description:(NSString *)description
                    group:(NSInteger) group;
- (NSInteger)numberOfTasksForToday;
@end
