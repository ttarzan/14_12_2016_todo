//
//  AppDelegate.m
//  ToDo
//
//  Created by Kurs on 12/14/16.
//  Copyright © 2016 Kurs. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate () <CLLocationManagerDelegate>

// sto ranije treba da trazimo lokacija korisnika
@property(strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation AppDelegate
#pragma mark - Properties
// ******************** GENERISAN CODE OD STRANE APPLE ZA CORE DATA
@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    @synchronized (self) {
        if (!_persistentContainer) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"ToDo"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription , NSError *error) {
                    if (error) {
                        NSLog(@"Unresolved Error %@ %@", error, error.userInfo);
                        abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Public API

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@ %@", error, error.userInfo);
        abort();
    }
}

//*************************************************


#pragma mark - Private API
- (void)configureLocationManager {
    
    if (![CLLocationManager locationServicesEnabled]) return;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // namestam koliko cesto ce se GPS update
    self.locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
    // kada ce app da kkoristi GPS.
    [self.locationManager requestWhenInUseAuthorization];
    // startuj se samo kada se dese neke velike promene, a u distanceFilter sam rekao sta je ta promena
    
    // druga linija koda je za simulator. inace ovako ide za device
    // [self.locationManager startMonitoringSignificantLocationChanges];
    // ovo je za simulator
    [self.locationManager startUpdatingLocation];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self configureLocationManager];
    
    
    // ako je user logovan prikazi mu Home
    // if the user is logged in show ContainerViewController
    if ([Helpers isLoggedIn]) {
        // Bundle je cela moja apllikacija upakovana sa svim fajlovima
        UIViewController *rootViewController = [Helpers initWithControllerFrom:@"ContainerViewController"];
        
        self.window.rootViewController = rootViewController;
    }
    return YES;
}

#pragma mark - CLLocationManagerDelegate
// momenat kada se lokacija update onda se poziva DataManager i tu setuje lokaciju
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (locations.count > 0) {
        DataManager *dataManager = [DataManager sharedManager];
        dataManager.userLocation = [locations lastObject];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}
@end
