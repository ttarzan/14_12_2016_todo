//
//  LoginViewController.h
//  ToDo
//
//  Created by mil on 12/14/16.
//  Copyright © 2016 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

//sve sto mi se nalazi u h to je public izmedju @interface i @end
@interface LoginViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *usernameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
//IBOutlet - interface Builder Outlet - ovo je dokaz da smo ga povezali sa interface
// confirmButton ti ne treba zato sto nista neces direktno da radis na dugmetuu
//@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmButtonLeadingConstraint;
@property (strong, nonatomic) NSDictionary *attributes;
- (void)configurePlaceholders;
- (void)configureTextField:(UITextField *)textField;
@end
