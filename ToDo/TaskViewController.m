//
//  TaskViewController.m
//  ToDo
//
//  Created by Kurs on 2/22/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "TaskViewController.h"
#import <MapKit/MapKit.h>



@interface TaskViewController () <UITextFieldDelegate, DataManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
//@property (weak, nonatomic) IBOutlet UIView *greenView;
//@property (weak, nonatomic) IBOutlet UIView *orangeView;
//@property (weak, nonatomic) IBOutlet UIView *purpleView;
@property (weak, nonatomic) IBOutlet UIImageView *selectorImageView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewsArray;
@property (nonatomic) NSInteger group;

@end

@implementation TaskViewController
#pragma mark - Actions
- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addButtonTapped:(UIButton *)sender {
    if ([self validationPassed]) {
        if(self.task) {
            self.task.heading = self.titleTextField.text;
            self.task.desc = self.descriptionTextField.text;
            self.task.group = self.group;
            self.task.latitude = DATA_MANAGER.userLocation.coordinate.latitude;
            self.task.longitude = DATA_MANAGER.userLocation.coordinate.longitude;
            
            [DATA_MANAGER updateObject:self.task];
        }else {
            [DATA_MANAGER saveTaskWithTitle:self.titleTextField.text description:self.descriptionTextField.text group:self.group];
        }
        
        [self backButtonTapped:nil];
    }
}
- (IBAction)groupButtonTapped:(UIButton *)sender {
    
    // sender.tag je tag obojenih dugmica. pogledaj u properties
    self.group = sender.tag;
    
//    CGPoint center = CGPointZero;
    
//    switch (sender.tag) {
//        case TaskGroupCompleted:
//            center = self.greenView.center;
//            break;
//        case TaskGroupNotCompleted:
//            center = self.orangeView.center;
//            break;
//        case TaskGroupInProgress:
//            center = self.purpleView.center;
//            break;
//        default:
//            break;
//    }
}

#pragma mark - Private API
- (BOOL)validationPassed {
    if (self.titleTextField.text.length == 0) {
        [self showAlertWithTitle:@"Error" andMessage:@"Please enter task title"];
        return NO;
    }
    
    if (self.descriptionTextField.text.length == 0) {
        [self showAlertWithTitle:@"Error" andMessage:@"Please enter task description"];
        return NO;
    }
    return YES;
}

- (void)configureUI {
    // moram da postavim odmah locality zato sto kada sam prvi put dosao na ovaj ekran, ako sam logovan, nece postaviti.
    self.locationLabel.text = DATA_MANAGER.userLocality;
    
    if(self.task) {
        self.titleTextField.text = self.task.heading;
        self.descriptionTextField.text = self.task.desc;
        self.group = self.task.group;
        [self.mapView addAnnotation:self.task];
        
        self.mapView.showsUserLocation = YES;
        [self zoomToCoordinate:self.task.coordinate];
    
    }else {
        self.group = TaskGroupNotCompleted;
        
        [self zoomToCoordinate:DATA_MANAGER.userLocation.coordinate];
    }
}

- (void)zoomToCoordinate:(CLLocationCoordinate2D) coordinate {
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(SPAN_DELTA, SPAN_DELTA));
    [self.mapView setRegion:region animated:YES];
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureUI];
    
    
    // Ako imas lokaciju daj mi naziv grada u kome se ti nalazis
    // Via delegate promena lokacije
//    DataManager *dataManager = [DataManager sharedManager];
    // delegat za promenu lokacije je ovaj VC, tj. TaskViewController
//    dataManager.delegate = self;
    
    // Via notification
    [[NSNotificationCenter defaultCenter] addObserverForName:LOCALITY_UPDATED_NOTIFICATION object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        self.locationLabel.text = DATA_MANAGER.userLocality;
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - DataManagerDelegate
- (void)dataManagerDidUpdateLocality {
    // ovde definisem sta delegat radi
    self.locationLabel.text = DATA_MANAGER.userLocality;
}

#pragma mark - Properties

- (void)setGroup:(NSInteger)group {
    _group = group;
    
    // Move selector to appropriate group
    for (UIView *view in self.viewsArray) {
        if(view.tag == group) {
            [UIView animateWithDuration:kAnimationDuration animations:^{
                self.selectorImageView.center = view.center;
            }];
        }
    }
}
@end
