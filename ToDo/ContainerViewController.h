//
//  ContainerViewController.h
//  ToDo
//
//  Created by Kurs on 3/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController
- (void)openViewController: (UIViewController *)viewController;
@end
