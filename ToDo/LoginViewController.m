//
//  LoginViewController.m
//  ToDo
//
//  Created by Kurs on 12/14/16.
//  Copyright © 2016 Kurs. All rights reserved.
//

#import "LoginViewController.h"
//#define kAnimationDuration 0.75f
#import "User.h"
#define kScreenSize [UIScreen mainScreen].bounds

// sve sto mi se nalazi u .m fajlu to je private i to izmedju @interface i @end
@interface LoginViewController()
// constraint koji cu koristiti da mogu da pomerim text fields na gore kada se pojavi keyboard
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldsContainerViewBottomConstraint;
@property (nonatomic) CGFloat bottomConstraint;
@property (weak, nonatomic) IBOutlet UIView * logoView;
@end

@implementation LoginViewController

#pragma mark - Public API
- (void)configurePlaceholders {
    self.attributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    [self configureTextField:self.usernameTextField];
    [self configureTextField:self.passwordTextField];
}

- (void)configureTextField:(UITextField *)textField {
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:self.attributes];
    
    textField.attributedPlaceholder = attributedString;
}

#pragma mark - Actions
// svi actions kojke pravim moram da ih pravim unutar implementations
// id je bilo sta (moze da bude NSString i NSInteger i bilo koji drugi tip). kompajler u hodu gleda provaljuje koja je kontrola.
// IBAction je u stvari void. fora je sto se sa njim oznacava da sam povezao kontrolu sa kodom.
- (IBAction)confirmButtonTapped {
    //[self printPlaceHolders];
    [self loginUser];
}

#pragma mark - Private API
/*
-(void)printPlaceHolders {
    NSLog(@"viewOnLoad %@", self.usernameTextField.placeholder);
    NSLog(@"viewOnLoad %@", self.passwordTextField.placeholder);
    
    NSLog(@"viewOnLoad %@", self.confirmButton.currentTitle);
    NSLog(@"viewOnLoad %@", self.confirmButton.titleLabel.text);
}
*/
-(void)loginUser {
    NSLog(@"Logging in with %@ and %@",
          self.usernameTextField.text,
          self.passwordTextField.text);
    
    User *user = [[User alloc]init];
    user.username = self.usernameTextField.text;
    user.password = self.passwordTextField.text;
    [Helpers saveCustomObjectToUserDefaults:user forKey:USER_UD];
    
    // sender je podatak koji zelim da posaljem segueu
    // u storyboardu sam rekao da ide na ovaj segue
    [self performSegueWithIdentifier:@"HomeSegue" sender:nil];
    
}

// nije validan vise zato sto sam uradio refactoring
/*
-(void)configurePlaceHolders {
    
    // moze i sa alloc init. ovo je samo skraceni nacin
    NSDictionary *attributes = @ {
        NSForegroundColorAttributeName :[UIColor whiteColor],\
        NSFontAttributeName:[UIFont boldSystemFontOfSize:20]
    };
    
    // drugi nacin
    //NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    
    
    NSAttributedString *usernameAttributedString = [[NSAttributedString alloc] initWithString:self.usernameTextField.placeholder attributes:attributes];
    NSAttributedString *passwordAttributedString = [[NSAttributedString alloc] initWithString:self.passwordTextField.placeholder attributes:attributes];
    
    self.usernameTextField.attributedPlaceholder = usernameAttributedString;
    self.passwordTextField.attributedPlaceholder = passwordAttributedString;
}
*/
-(void)registerForNotifications {
    // defaultCenter je kao Singleton u JAvi - Android
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        [UIView animateWithDuration:kAnimationDuration animations:^{
            [self.view layoutIfNeeded];
        }];
        
        self.textFieldsContainerViewBottomConstraint.constant = 2 * self.bottomConstraint;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        [UIView animateWithDuration:kAnimationDuration animations:^{
            [self.view layoutIfNeeded];
        }];
        
        self.textFieldsContainerViewBottomConstraint.constant = self.bottomConstraint;
        
        // pozivanje metoda sam prebacio u animaciju
        //[self.view layoutIfNeeded];
    }];
}

#pragma mark - UIResponder
// metod sa kojim mogu da spustim tastaturu pipanjem bilo gde na ekran.
// UITouch su koordinate
// withEvent je event koji sam uradio prilikom pipanja ekrana.
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - View lifecycle
-(void)viewDidLoad {
    [super viewDidLoad];
    //[self printPlaceHolders];
    [self configurePlaceholders];
    [self registerForNotifications];
    
    self.bottomConstraint = self.textFieldsContainerViewBottomConstraint.constant;
    
    self.logoView.layer.cornerRadius = self.logoView.frame.size.width / 2;
    
}

// trenutak kada je view kompletno zavrsen po contrainst kakav sam zadao. u tom momentu mi daj constraint
// ovaj metod se uvek poziva koji god event da se desi na formi
-(void)viewDidLayoutSubviews {
    NSLog(@"Okinuta metoda");
    // uzimam constant za ovaj constraint. to je constraint koji se odnosi na donji deo od view-a koji sadrzi u sebi textfields za username i password.
    // property koji sam napravio za textFieldsContainerViewBottomConstraint vezao sam tako sto sam uradio desni klik na login view controller i vezao sa tom view controlom.
    //self.bottomConstraint = self.textFieldsContainerViewBottomConstraint.constant;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CGRect frame = [UIScreen mainScreen].bounds;
    self.confirmButtonLeadingConstraint.constant = frame.size.width;
    [self.view layoutIfNeeded];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.confirmButtonLeadingConstraint.constant = 0.0f;
    [UIView animateWithDuration:kAnimationDuration
                     animations:^{
                         self.logoView.alpha = 0.0f;
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    //self.textFieldsContainerViewBottomConstraint.constant = 2 * self.bottomConstraint;
    NSLog(@"Poceo je neko da me edituje");
    if(textField == self.usernameTextField) {
        self.usernameImageView.image = [UIImage imageNamed:@"username-active"];
    }else if(textField == self.passwordTextField) {
        self.passwordImageView.image = [UIImage imageNamed:@"password-active"];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"Prestao je neko da me edituje");
    self.usernameImageView.image = [UIImage imageNamed:@"username"];
    self.passwordImageView.image = [UIImage imageNamed:@"password"];
}

// regulisem da li se keyboard pojavljjuje ili ne.
// 
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
        //self.textFieldsContainerViewBottomConstraint.constant = self.bottomConstraint;
        // ne spusta keyboard vec ga ostalja visible dok sam u username field-u
        return NO;
    }else if (textField == self.passwordTextField){
        [textField resignFirstResponder];
    }
    return YES;
}

@end
