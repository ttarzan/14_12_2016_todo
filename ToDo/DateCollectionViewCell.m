//
//  DateCollectionViewCell.m
//  ToDo
//
//  Created by Kurs on 2/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "DateCollectionViewCell.h"

@interface DateCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *weekdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIView *hightlightView;
@property (weak, nonatomic) IBOutlet UIView *hasTaskView;
@end

@implementation DateCollectionViewCell

#pragma mark - Properties
- (void)setDate:(NSDate *)date {
    _date = date;
    self.dayLabel.text = [Helpers valueFrom:date
                                 withFormat:@"d"];
    self.weekdayLabel.text = [self weekday];
    
    self.hightlightView.hidden = [Helpers isDate:date sameAsDate:[NSDate date]] ? NO : YES;
}
#pragma mark - Private API
- (NSString *)weekday {
    // umesto EEEE moze da se stavi EEE. Tada umesto SATURDAY daje SAT
    NSString *string = [Helpers valueFrom:self.date withFormat:@"EEEE"];
    string = string.uppercaseString;
    string = [string substringToIndex:3];
    return string;
}
@end
