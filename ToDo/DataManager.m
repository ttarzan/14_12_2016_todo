//
//  DataManager.m
//  ToDo
//
//  Created by Kurs on 2/25/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"

@interface DataManager()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation DataManager
#pragma mark - Properties
- (void)setUserLocation:(CLLocation *)userLocation {
    _userLocation = userLocation;
    
    // CLGeocoder radi reverzno trazenje iz lat i long da dobijes mesto u kojem je korisnik
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if(error) {
            NSLog(@"CLGeocoder: %@", error.localizedDescription);
        }
        
        if (placemarks.count > 0) {
            CLPlacemark *placemark = [placemarks firstObject];
            
            self.userLocality = placemark.locality;
            /*
            NSLog(@"ISOCountryCode: %@", placemark.ISOcountryCode);
            NSLog(@"Country: %@", placemark.country);
            NSLog(@"Postal Code: %@", placemark.postalCode);
            NSLog(@"Administrative Area: %@", placemark.administrativeArea);
            NSLog(@"Locality: %@", placemark.locality);
            NSLog(@"SubLocality: %@", placemark.subLocality);
            NSLog(@"Sub thoroughface: %@", placemark.subThoroughfare);
             */
        }
    }];
    
}
- (void)setUserLocality:(NSString *)userLocality {
    _userLocality = userLocality;
    
    // Via delegate obavestavanje o promeni lokacije
    if(self.delegate) {
        [self.delegate dataManagerDidUpdateLocality];
    }
    
    // Via notification obavestavanje o promeni lokacije
    [[NSNotificationCenter defaultCenter] postNotificationName:LOCALITY_UPDATED_NOTIFICATION object:nil];
}

// Ovde mi se cuvaju svi privremeni podaci koji se odnose na DB
// svaki objekat u bazi ima managedObjectContext sto znaci da zna u kom se contextu nalazi uvek
// getter
- (NSManagedObjectContext *)managedObjectContext {
    if (!_managedObjectContext) {
        // zato sto je SINGLETON (sharedApplication) ja UVEK ZOVEM ISTU INSTANCU. nema veze da li je pravim opet
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _managedObjectContext = appDelegate.persistentContainer.viewContext;
    }
    
    return _managedObjectContext;
}

#pragma mark - Public API
// Dobra fora je sto je ovo genericki, entityName je nesto sto mogu da menjam
- (NSMutableArray *)fetchEntity:(NSString *)entityName withFilter:(NSString *)filter withSortAsc:(BOOL)sortAscending forKey:(NSString *)sortKey{
    // Dohvatamo podatke iz DB
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Govorim koji entitet hocu. Napravi mi entityDescitpyion za odredjeni entitet u odredjenom contextu
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    // fetchuj mi podatke za njega
    [fetchRequest setEntity:entityDescription];
    
    // Sorting
    // ASC, DESC
    // sortKey je naziv kolone, ili atribut
    if (sortKey) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
        // @[] skracenica za niz
        NSArray *sortDescriptors = @[sortDescriptor];
        [fetchRequest setSortDescriptors:sortDescriptors];
        // [fetchRequest setSortDescriptors:@[sortDescriptor]];
    }
    
    // Filtering
    // WHERE in SQL i slicni filteri.
    if( filter != nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter];
        [fetchRequest setPredicate:predicate];
    }
    
    // Execute fetch request
    NSError *error;
    // &error fetchRequest krene i ako ima error onda ce da ga upise u error objekat.
    NSArray *resultsArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(error) NSLog(@"Error fetching %@(s).", error.localizedDescription);
    return [resultsArray mutableCopy];
}

- (void)deleteObject:(NSManagedObject *)object {
    [self.managedObjectContext deleteObject:object];
    // PRVI NACIN
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([appDelegate respondsToSelector:@selector(saveContext)]) {
        [appDelegate saveContext];
    }
    
    // DRUGI NACIN
    // save:nil je za error. ne interesuje me.
//    [object.managedObjectContext save:nil];
    
}

- (void)updateObject:(NSManagedObject *)object {
    
    // PRVI NACIN
    // svaki objekat ima managedObjectContext
    NSError *error = nil;
    if ([object.managedObjectContext hasChanges] && ![object.managedObjectContext save:&error]){
        NSLog(@"Error updating object in database: %@ %@", error.localizedDescription, error.userInfo);
        abort();
    }
    
    // DRUGI NACIN
//    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    if ([appDelegate respondsToSelector:@selector(saveContext)]) {
//        [appDelegate saveContext];
//    }
    
    // TRECI NACIN
//    [object.managedObjectContext save:nil];
}

- (void)logObject:(NSManagedObject *)object {
    // Uzimam opis entiteta
    NSEntityDescription *description = [object entity];
    // Uzimam sve atribute po imenu i ubacim u dictionary. Atribut je kolona u db.
    NSDictionary *attributes = [description attributesByName];
    
    // Uzimam jedan po jedan atribut iz dictionary
    // za svaki atribut idu u bazu i uzima vrednost
    for (NSString *attribute in attributes) {
        NSLog(@"%@ = %@", attribute, [object valueForKey:attribute]);
        NSLog(@"%@ = %@", attribute, attributes[attribute]);
    }
}


- (NSInteger)numberOfTasksPerTaskGroup:(TaskGroup)group {
    // Koliko imamo taskova pod tom grupom
    NSArray *tasksArray = [self fetchEntity:NSStringFromClass([DBTask class]) withFilter:[NSString stringWithFormat:@"group = %ld", group] withSortAsc:NO forKey:nil];
    
    return tasksArray.count;
}

- (NSInteger)numberOfTasksForToday {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMAT;
    
    NSString *filter = [NSString stringWithFormat:@"date LIKE '%@'", [dateFormatter stringFromDate:[NSDate date]]];
    
    NSMutableArray *tasksArray = [self fetchEntity:NSStringFromClass(DBTask.class) withFilter:filter withSortAsc:YES forKey:@"date"];
    
    return tasksArray.count;
}

- (void)saveTaskWithTitle:(NSString *)title description:(NSString *)description group:(NSInteger)group {
    DBTask *task = (DBTask *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DBTask class]) inManagedObjectContext:self.managedObjectContext];
    task.heading = title;
    task.desc = description;
    
    if(self.userLocation) {
        task.latitude = self.userLocation.coordinate.latitude;
        task.longitude = self.userLocation.coordinate.longitude;
    }
    
    // Date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMAT;    
    task.date = [dateFormatter stringFromDate:[NSDate date]];
    
    task.group = group;
    
    // Save
    // save:nil mi je za error. kada krenes da snimas ako ima error ne interesuje me
    [task.managedObjectContext save:nil];
    
}

#pragma mark - Private API

#pragma mark - Deginated Initializer
+ (instancetype)sharedManager {
    // DataManager je naziv klase koju hocu da bude SIngleton.
    // Umesto DataManager ja stavio koju zelim klasu da mi bude Singleton
    // Kada instanciram DataManager objekat, on podize sve njegove property-je
    // Prvi put kada neko pozove sharedManager onda ce da kreira instancu
    static DataManager *shared;
    
    // synchronized je najbolji nacin za pisanje singleton-a
    // odnosi se sa na threading
    @synchronized (self) {
        if (shared == nil) {
            shared = [[DataManager alloc] init];
        }
    }
    
    return shared;
}
@end
