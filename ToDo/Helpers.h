//
//  Helpers.h
//  ToDo
//
//  Created by Kurs on 2/15/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helpers : NSObject
+ (BOOL)isMorning;
+ (NSString *)valueFrom:(NSDate *)date withFormat:(NSString *)format;
+ (NSInteger)numberOfDaysInMonthForDate:(NSDate *)date;
+ (BOOL)isDate:(NSDate *)date sameAsDate:(NSDate *)otherDate;
// snima custom objekat u NSUserDefaults
// key je kljuc pod kojim zelim da snimim custom objekat
// object je instanca klase koju zelim da ubacim u NSUserDefaults
+ (void)saveCustomObjectToUserDefaults:(id)object forKey:(NSString *)key;
// load custom objekat iz NSUserDefaults
+ (id)loadCustomObjectFromUserDefaultsForKey:(NSString *)key;
+ (BOOL)isLoggedIn;
+ (UIViewController *)initWithControllerFrom:(NSString *)storyboardID;
@end
