//
//  WalkthroughCollectionViewCell.h
//  ToDo
//
//  Created by Kurs on 3/24/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalkthroughCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
