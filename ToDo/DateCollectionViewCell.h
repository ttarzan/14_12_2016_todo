//
//  DateCollectionViewCell.h
//  ToDo
//
//  Created by Kurs on 2/17/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) NSDate *date;

@end
