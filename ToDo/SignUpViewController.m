//
//  SignUpViewController.m
//  ToDo
//
//  Created by Kurs on 12/30/16.
//  Copyright © 2016 Kurs. All rights reserved.
//

#import "SignUpViewController.h"
#import "User.h"
#define kToolBarHeight 44.0f

@interface SignUpViewController()
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *birthdayImageView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIToolbar *toolbar;
@property (strong,nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation SignUpViewController

#pragma mark - Properties

// getter u ObjC za datePicker

- (UIDatePicker *)datePicker {
    if(!_datePicker) {
        _datePicker = [[UIDatePicker alloc]init];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.backgroundColor = [UIColor darkGrayColor];
        _datePicker.tintColor = [UIColor whiteColor];
        // OVO DOLE JE ISTO KAO I GORE
        // _datePicker.backgroundColor = UIColor.darkGrayColor;
        // _datePicker.tintColor = UIColor.whiteColor;
    }
    
    return _datePicker;
}

// getter u ObjC za toolbar
- (UIToolbar *)toolbar {
    if(!_toolbar) {
        // action:@selector je metod koji ce se pokrenuti kada se klikne na dugme done.
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped)];
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        CGRect frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, kToolBarHeight);
        _toolbar = [[UIToolbar alloc]initWithFrame:frame];
        
        // ova dva dole su ista
        //toolbar.items = @[flexibleSpace, doneButton];
        [_toolbar setItems:@[flexibleSpace, doneButton]];
    }
    return _toolbar;
}

// getter u ObjC za dateFormatter
- (NSDateFormatter *)dateFormatter {
    if(!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc]init];
        _dateFormatter.dateFormat = @"dd.MM.yyyy";
    }
    return _dateFormatter;
}

#pragma bar - Actions
- (void)confirmButtonTapped {
    [self signUpUser];
}

-(IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneButtonTapped {
    self.birthdayTextField.text = [self.dateFormatter stringFromDate:self.datePicker.date];
    [self.birthdayTextField resignFirstResponder];
}

- (void)swipeRight {
    [self backButtonTapped:nil];
}

#pragma bar - Private API
- (void)signUpUser {
    NSLog(@"SignUp in with %@,%@,%@ and %@",
          self.usernameTextField.text,
          self.passwordTextField.text,
          self.emailTextField.text,
          self.birthdayTextField.text
          );
    
    User *user = [[User alloc]init];
    user.username = self.usernameTextField.text;
    user.password = self.passwordTextField.text;
    user.email = self.emailTextField.text;
    user.birthday = self.birthdayTextField.text;
    
    [Helpers saveCustomObjectToUserDefaults:user forKey:USER_UD];
    
    // sender je podatak koji zelim da posaljem segueu
    // u storyboardu sam rekao da ide na ovaj segue
    [self performSegueWithIdentifier:@"HomeSegue" sender:nil];
}
-(void)configurePlaceholders {
    [super configurePlaceholders];    
    [self configureTextField:self.emailTextField];
    [self configureTextField:self.birthdayTextField];
}

- (void)configureDatePickerInputView {
    self.birthdayTextField.inputView = self.datePicker;
    self.birthdayTextField.inputAccessoryView = self.toolbar;
}

#pragma mark - UIResponder
// metod sa kojim mogu da spustim tastaturu pipanjem bilo gde na ekran.
// UITouch su koordinate
// withEvent je event koji sam uradio prilikom pipanja ekrana.
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
#pragma bar - View lifecycle
// LIFECYCLE CHILD-a AUTOMATSKI POZIVA LIFECYCLE OD RODITELJA
-(void) viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeGestureRecognizer];
}

#pragma bar - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [super textFieldDidBeginEditing:textField];
    
    if(textField == self.emailTextField) {
        self.emailImageView.image = [UIImage imageNamed:@"email-active"];
    }else if(textField == self.birthdayTextField) {
        self.birthdayImageView.image = [UIImage imageNamed:@"birthday-active"];
        [self configureDatePickerInputView];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super textFieldDidEndEditing:textField];
    
    self.emailImageView.image = [UIImage imageNamed:@"email"];
    self.birthdayImageView.image = [UIImage imageNamed:@"birthday"];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if (textField == self.usernameTextField) {
        [self.emailTextField becomeFirstResponder];
        return NO;
    }else if(textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }else if (textField == self.passwordTextField) {
        [self.birthdayTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

@end
