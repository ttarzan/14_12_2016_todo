//
//  Constants.h
//  ToDo
//
//  Created by Kurs on 2/24/17.
//  Copyright © 2017 Kurs. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

typedef NS_ENUM(NSInteger, TaskGroup) {
    TaskGroupCompleted = 1,
    TaskGroupNotCompleted,
    TaskGroupInProgress    
};

#define kAnimationDuration 0.3f
#define kMenuOffset 100.f
#define SPAN_DELTA 0.5

// Colors
#define kColorCompleted [UIColor colorWithRed:73.0/255.0 green:211.0/255.0 blue:194.0/255.0 alpha:1.0]
#define kColorNotCompleted [UIColor colorWithRed:254.0/255.0 green:172.0/255.0 blue:73.0/255.0 alpha:1.0]
#define kColorInProgress [UIColor colorWithRed:187.0/255.0 green:114.0/255.0 blue:255.0/255.0 alpha:1.0]

// Notificatins
static NSString *const LOCALITY_UPDATED_NOTIFICATION = @"LOCALITY_UPDATED_NOTOFICATION";
static NSString *const OPEN_SIDE_MENU_NOTIFICATION = @"OPEN_SIDE_MENU_NOTIFICATION";
static NSString *const CLOSE_SIDE_MENU_NOTIFICATION = @"CLOSE_SIDE_MENU_NOTIFICATION";
static NSString *const OPEN_VC_NOTIFICATION = @"OPEN_VC_NOTIFICATION";


// User Defaults
// isto kao i USER_UD samo za image
static NSString *const USER_IMAGE = @"USER_IMAGE";
// key koji cu napraviti u NSUserDefaults u kojem se nalazi podatak koji trazim
static NSString *const USER_UD = @"USER_UD";

// Macros
#define DATA_MANAGER [DataManager sharedManager]

static NSString *const DATE_FORMAT = @"yyyy-MM-dd";
#endif /* Constants_h */
