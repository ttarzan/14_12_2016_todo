//
//  HomeViewController.swift
//  VestiS
//
//  Created by Kurs on 3/31/17.
//  Copyright © 2017 kurs. All rights reserved.
//

import UIKit

let apiUrl = "http://www.brzevesti.net/api/news"

class HomeViewController: UIViewController {
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    var articles = [Article]()
    
    // MARK: - Public API
    func loadArticles() {
        guard isConnected() else {
            return }
        
        // .userInitiated je enum
        // kod Swift-a ne mora da se kuca naziv Enum-a pa naziv, vec je samo dovoljno da se kuca tacka pa naziv enuma
        
        if let url = URL(string: apiUrl) {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            session.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    // localizedDescription je lokalizovani error. na kojem jeziku ti je telefon, na tom jeziku pise error
                    print("Error: \(error!.localizedDescription)")
                }
                
                if data != nil {
                    
                }
            })
        }
        
    }
    
    // MARK: - VIew lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // ubijem bele crtice koje se pojavljuju kada nema sadrzaja u celijama
        tableView.tableFooterView = UIView()
        loadArticles()
    }
}


extension HomeViewController: UITableViewDataSource,UITableViewDelegate {
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView)->Int {
        return 1
    }
    
    func tableView (_ tableView:UITableView, numberOfRowsInSection section: Int)->Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // as je kastovanje
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ArticleTableViewCell
        cell.article = articles[indexPath.row];
        
        return cell
    }
    
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
