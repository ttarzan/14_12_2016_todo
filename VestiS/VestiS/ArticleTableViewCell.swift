//
//  ArticleTableViewCell.swift
//  VestiS
//
//  Created by Kurs on 3/31/17.
//  Copyright © 2017 kurs. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    // MARK: - Properties
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var portalLabel:UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    
    // property
    // setter overriding
    // kad te neko setuje, iz objekta mi izvuci title, portal i slicno
    // objc - @property (strong, nonatomic) Article *article;
    var article: Article! {
        // setter overrindg
        didSet {
            titleLabel.text = article.title
            portalLabel.text = article.portal
            coverImageView.layer.cornerRadius = coverImageView.frame.size.width / 2
            coverImageView.loadImage(from: article.imageUrl)
        }
    }
    
}

