//
//  Article.swift
//  VestiS
//
//  Created by Kurs on 3/31/17.
//  Copyright © 2017 kurs. All rights reserved.
//

import Foundation

class Article {
    var desc = ""
    var imageUrl = ""
    var portal = ""
    var title = ""
    var url = ""
    var region = ""
}
